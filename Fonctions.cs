using System;

namespace AlgoAvanceeFonctions
{
    public class Codebarre
    {
        private string scan;

        public Codebarre(string scan)
        {
            this.scan = scan;
        }
        public string GetScan()
        {
            return this.scan;
        }
        public void GetTypeOfProduct(string code_result, string[] rdm_tab)
        {
            string type_of_product="";
            int i=0;

            do{
                    type_of_product = type_of_product + code_result[i];
                    i++;
            }while(code_result[i] != '-');
                
            rdm_tab[0] = type_of_product;
            rdm_tab[1] = i.ToString();
        }

        public void GetYearOfProduct(string code_result, string[] rdm_tab2, string[] date_tab)
        {
            string date = "";
            string temp = date_tab[1];
            int i= int.Parse(temp) + 1;

            do{
                date = date + code_result[i] ;
                i++;
            }while(code_result[i] != '-');

            rdm_tab2[0] = date;
            rdm_tab2[1] = i.ToString();
        }

        public void GetRefOfProduct(string code_result, string[] rdm_tab3, string[] ref_tab)
        {
            string referency = "";
            string temp = ref_tab[1];
            int i= int.Parse(temp) + 1;

            do{
                referency = referency + code_result[i];
                i++;
            }while(i < code_result.Length);

            rdm_tab3[0] = referency;
            rdm_tab3[1] = i.ToString();
        }
        public void Affiche(string[] type, string[] date, string[] refe)
        {
            Console.WriteLine("Type du produit : {0}" , type[0]);
            Console.WriteLine("Annee du produit : {0}" , date[0]);
            Console.WriteLine("Reference du produit : {0}" , refe[0]);
        }
            
        public string VerificationPremierPaquet(string[] type_produit)
        {
            string valide = "true";
                
            if(type_produit[0].Length != 3)
            {
                valide = "false";
                Console.WriteLine("le premier paquet n'est pas au bon format : 3 lettres en majuscules");
            }
            else{
                valide = "true";
            }

            return valide;
        }
            
        public string VerificationDeuxiemeEtTroisiemePaquet(string[] date_produit, string[] ref_produit)
        {
            string valide = "true";
                
            if(date_produit[0].Length != 4 || ref_produit[0].Length != 4)
            {
                valide = "false";
                Console.WriteLine("L'un des arguments date ou reference du produit n'est pas au bon format");
            }
            else{
                valide = "true";
            }

            return valide;
        }
            
        public string VerificationDateProduit(string[] date_product)
        {
            string valide = "true";

            if(int.Parse(date_product[0]) != DateTime.Now.Year)
            {
                valide = "false";
                Console.WriteLine("Date du produit invalide");
            }
            else{
                valide = "true";
            }

            return valide;
        }
            
        public string RetourLectureCodeBarre(string result)
        {
            Console.WriteLine("Veuillez ressaisir les informations du produit au bon format : separation par un -");
            result = Console.ReadLine();

            return result;
        }    
    }
}