﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DouchetteFonctions;

namespace Verification
{
    class Program
    {
        static void Main(string[] args)
        {
            CodeBarre test = new CodeBarre("r-21-dom");

            //tableaux contenant les infos du produit
            string[] tab_for_type = new string[2];
            string[] tab_for_year = new string[2];
            string[] tab_for_ref = new string[2];

            //variable permettant de stocker les resultats des fonctions de verification
            string v1 = "", v2 = "", v3 = "";

            //resultat du scan
            string scan_res = test.GetScan();

            test.GetTypeOfProduct(scan_res, tab_for_type);
            v1 = test.VerificationPremierPaquet(tab_for_type);
            test.GetYearOfProduct(scan_res, tab_for_year, tab_for_type);
            v2 = test.VerificationDateProduit(tab_for_year);
            test.GetRefOfProduct(scan_res, tab_for_ref, tab_for_year);
            v3 = test.VerificationDeuxiemeEtTroisiemePaquet(tab_for_year, tab_for_ref);

            while (v1 != "true" || v2 != "true" || v3 != "true")
            {
                if (v1 != "true" || v2 != "true" || v3 != "true")
                {
                    scan_res = test.RetourLectureCodeBarre(scan_res);

                    test.GetTypeOfProduct(scan_res, tab_for_type);
                    v1 = test.VerificationPremierPaquet(tab_for_type);

                    test.GetYearOfProduct(scan_res, tab_for_year, tab_for_type);
                    v2 = test.VerificationDateProduit(tab_for_year);

                    test.GetRefOfProduct(scan_res, tab_for_ref, tab_for_year);
                    v3 = test.VerificationDeuxiemeEtTroisiemePaquet(tab_for_year, tab_for_ref);
                }
            }

            test.Affiche(tab_for_type, tab_for_year, tab_for_ref);
        }
    }
}
